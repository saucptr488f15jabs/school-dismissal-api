class SchoolController < ApplicationController
	skip_before_filter :authenticate_user!
  def register
		@reg = School.new
		@states = {AK: "Alaska", AL: "Alabama", AR: "Arkansas", AS: "American Samoa", AZ: "Arizona", CA: "California", CO: "Colorado", CT: "Connecticut", DC: "District of Columbia", DE: "Delaware", FL: "Florida", GA: "Georgia", GU: "Guam", HI: "Hawaii", IA: "Iowa", ID: "Idaho", IL: "Illinois", IN: "Indiana", KS: "Kansas", KY: "Kentucky", LA: "Louisiana", MA: "Massachusetts", MD: "Maryland", ME: "Maine", MI: "Michigan", MN: "Minnesota", MO: "Missouri", MS: "Mississippi", MT: "Montana", NC: "North Carolina", ND: "North Dakota", NE: "Nebraska", NH: "New Hampshire", NJ: "New Jersey", NM: "New Mexico", NV: "Nevada", NY: "New York", OH: "Ohio", OK: "Oklahoma", OR: "Oregon", PA: "Pennsylvania", PR: "Puerto Rico", RI: "Rhode Island", SC: "South Carolina", SD: "South Dakota", TN: "Tennessee", TX: "Texas", UT: "Utah", VA: "Virginia", VI: "Virgin Islands", VT: "Vermont", WA: "Washington", WI: "Wisconsin", WV: "West Virginia", WY: "Wyoming" }.values
  end

	def save
		params[:school][:configured] = false
		s = School.new(school_params)
		u = User.new(name: params[:school][:owner], email: params[:school][:email], password: params[:school][:password], password_confirmation: params[:school][:password_confirmation])
		begin
			if s.save! and u.save!
				s.update(code: s.code.downcase)
				Role.all.each do |r|
					u.roles << r
				end
				u.schools << s
				respond_to do |format|
					format.html { redirect_to "http://#{params[:school][:code]}.myschooldismissal.com" }
					format.json { render json: 'OK' and return }
				end
			else
				render inline: "Oops, something seems to have gone wrong... Sorry... To report this bug, please click <a href=\"https://bugs.myschooldismissal.com/projects/website/issues/new\">here</a>, Thanks.".html_safe and return
			end
		rescue ActiveRecord::RecordInvalid => e
			render plain: 'dupe email' and return
		end
	end

	def check_code
		codes = School.all.collect{|s| s.code}
		render plain: params[:code] and return unless codes.include? params[:code]
		idk = Digest::MD5.hexdigest("#{Time.now} #{params[:code]}")
		render plain: "#{params[:code]}#{idk[0..3].upcase}"
	end

	private

	def school_params
		params.require(:school).permit(:name, :owner, :email, :address, :city, :state, :zip, :phone, :code)
	end
end
