class PickUpController < ApplicationController
  def index
    @students = []
    @vehicles = User.find_by(email: current_user.email).vehicles
    @locations = Location.all
    user = User.find_by(email: current_user.email)
    @default_vehicle = Drive.find_by(user_id: current_user.id, default: true)
    @default_vehicle = @default_vehicle.vehicle_id unless @default_vehicle.nil?
    @default_location = user.location_id
    @default_students = user.default_guarding_students.collect{|s| s.id}
    user.guarding_students.sort_by{|s| s.name}.each do |s|
      obj = {}
      obj[:name] = s.name
      student = History.where('created_at >= ?', Time.zone.now.beginning_of_day).select{|hs| hs.student_id == s.id}.last

      obj[:guardian_id] = user.id
      obj[:action] = 'Picking Up'
      obj[:student_id] = s.id

      if student.nil?
        obj[:status] = 'Pick Up'
        obj[:location_id] = 'None'
        obj[:vehicle_id] = 'None'
      else
        obj[:status] = student.status
        obj[:location_id] = Location.find_by(id: student.location_id).name
        obj[:vehicle_id] = Vehicle.find_by(id: student.vehicle_id).description
      end
      @students << obj
    end
  end

	def update
    render plain: 'ERROR: no students selected' and return if params[:students].nil?
    render plain: 'ERROR: no vehicle selected' and return if params[:vehicle].nil?
    render plain: 'ERROR: no location selected' and return if params[:location].nil?
    render plain: "ERROR: vehicle #{params[:vehicle]} not found!" and return if Vehicle.find_by(id: params[:vehicle]).nil?
    render plain: "ERROR: location #{params[:location]} not found!" and return if Location.find_by(name: params[:location]).nil?
    render plain: "ERROR: location #{params[:location]} not found!" and return if Location.find_by(name: params[:location]).nil?
    students = JSON.parse(params[:students])
    status = 'Picking Up' if params[:status].nil?

    students.each do |s|
      render plain: "Student #{s} not found!" and return if Student.find_by(id: s).nil?
      @history = History.new(student_id: s, user_id: User.find_by(email: current_user.email).id, vehicle_id: params[:vehicle], location_id: Location.find_by(name: params[:location]).id, status: status)
      unless @history.save
        respond_to do |format|
          format.html {
            render js: 'alert("ERROR");' and return
          }
          format.json {
            render json: {'status': "ERROR saving ''${s}''"} and return
          }
        end
      end
    end

    respond_to do |format|
      format.html {
        render inline: "<script type=\"text/javascript\">alert('Status updated successfully'); window.history.back();</script>"
      }
      format.json {
        render json: {'status': 'Your child should be out shortly.'}
      }
    end
  end

	def teacher
    @students = []
    teacher = User.find_by(email: current_user.email)
    render plain: 'You are not a teacher', status: 401 and return unless teacher.roles.collect{|r| r.role}.include?('Teacher')
    teacher.teaching_students.sort_by{|s| s.name}.each do |s|
      obj = {}
      obj[:id] = s.id
      obj[:name] = s.name
      student = History.where('created_at >= ?', Time.zone.now.beginning_of_day).select{|hs| hs.student_id == s.id}.last
      obj[:status] = 'Pick Up'
      obj[:guardian] = 'None'
      obj[:location] = 'None'
      unless student.nil?
        obj[:status] = student.status
        obj[:guardian] = User.find_by(id: student.user_id).name unless User.find_by(id: student.user_id).nil?
        obj[:location] = Location.find_by(id: student.location_id).name unless Location.find_by(id: student.location_id).nil?
      end
      @students << obj
    end
    respond_to do |format|
      format.html {
        redirect_to root_path and return #if params[:ajax].nil?
#        render layout: false
      }
      format.json {
        render json: @students
      }
    end
  end

  def attendance
    params[:present].each do |p|
      student = History.where('created_at >= ?', Time.zone.now.beginning_of_day).select{|h| h.student_id == p}.last
      if student.nil?
        #History.create(student_id: p, status: 'present')
      else
        student.update(status: 'present')
      end
    end unless params[:present].nil?
    params[:absent].each do |a|
      student = History.where('created_at >= ?', Time.zone.now.beginning_of_day).select{|h| h.student_id == a}.last
      if student.nil?
        History.create(student_id: a, status: 'absent')
      else
        student.update(status: 'absent')
      end
    end unless params[:absent].nil?
    render plain:'ok'
  end
end
