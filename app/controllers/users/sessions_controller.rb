class Users::SessionsController < Devise::SessionsController

def render_json
end

def create
	respond_to do |format|
		format.html {
			super
		}
		format.json {
			user = warden.authenticate!(auth_options)
			token = Tiddle.create_and_return_token(user, request)
      roles = User.find_by(email: current_user.email).roles.collect{|r| r.role}
			render json: { authentication_token: token, token: token, roles: roles, rolls: roles }
		}
	end
end

def destroy
	respond_to do |format|
		format.html { super }
		format.json {
			Tiddle.expire_token(current_user, request) if current_user
			render json: {"status": "singned out"}
		}
	end
end

private

# this is invoked before destroy and we have to override it
def verify_signed_out_user
end


# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
