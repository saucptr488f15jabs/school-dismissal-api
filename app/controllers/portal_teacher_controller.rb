class PortalTeacherController < ApplicationController
	before_filter :verify_role
	
	def verify_role
		redirect_to root_path unless current_user.roles.include? Role.find(2)
	end


  def index
		redirect_to "/portal/teacher/#{Role.find(2).portals.first.name}"
  end

  def students
    @students = []
    User.find_by(email: current_user.email).teaching_students.each do |s|
      student = {}
      student[:id] = s.id
      student[:name] = s.name
      student[:status] = 'Pick Up'
      student[:location] = 'None'
      history = History.where('created_at >= ?', Time.zone.now.beginning_of_day).select{|hs| hs.student_id == s.id}.last
      unless history.nil?
        student[:status] = history.status
        student[:location] = history.location.name unless history.location.nil?
      end
      @students << student # if s.status == 'En Route'
    end
    @students.sort_by!{|k| k[:name]}
    respond_to do |format|
      format.html { render 'students' }
      format.json { render json: {"students": @students} }
    end
  end

  def students_new
    @student = Student.new
  end

  def students_create
    s = Student.find_or_create_by(name: params[:student][:name])
    current_user.teaching_students << s
    StudentUser.where('student_id = ? and user_id = ?', s.id, current_user.id).first.update(confirmed: true)
    respond_to do |format|
      format.html { redirect_to portal_teacher_students_path }
      format.json { render json: {status: 'OK'} }
    end
  end

  def status
  end

  def reset
    current_user.teaching_students.each do |student|
      History.create(student_id: student.id, user_id: current_user.id, status: 'In Room')
    end
    render plain: 'OK' and return
  end

  def update
    render plain: 'ERROR: no student selected' and return if params[:student_id].nil?
    render plain: "ERROR: student #{params[:student_id]} not found!" and return if Student.find_by(id: params[:student_id]).nil?
    if params[:status].nil?
      status = 'Pick Up'
    else
      status = params[:status]
    end

		Student.find(params[:student_id]).update(status: params[:status])
    @history = History.new(student_id: params[:student_id], user_id: User.find_by(email: current_user.email).id, status: status)
    if @history.save
      respond_to do |format|
        format.html {
          render inline: "<script type=\"text/javascript\">alert('Status updated successfully'); window.history.back();</script>"
        }
        format.json {
          render json: {'status': 'OK'}
        }
      end
    else
      respond_to do |format|
        format.html {
          render js: 'alert("ERROR");' and return
        }
        format.json {
          render json: {'status': "ERROR saving ''${s}''"} and return
        }
      end
    end
  end
end
