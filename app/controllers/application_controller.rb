class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
	# before_action :testing
  protect_from_forgery except: :null_session
	before_filter :set_portals
	skip_before_filter :verify_authenticity_token
	before_action :school_code
	before_action :authenticate_user!
	after_action :allow_iframe

	def is_local?
		true if current_user.email.end_with?('@localhost.local')
	end

	def testing
		logger.warn "*** BEGIN RAW REQUEST HEADERS ***"
		self.request.env.each do |header|
			if header[0].starts_with? 'HTTP_X'
				logger.warn "HEADER KEY: #{header[0]}"
				logger.warn "HEADER VAL: #{header[1]}"
			end
		end
		logger.warn "*** END RAW REQUEST HEADERS ***"
	end

	def set_portals
		@portals = []
		return if current_user.nil?
		roles = User.find_by(email: current_user.email).roles
		if roles.empty?
			User.find_by(email: current_user.email).roles << Role.find(1)
			roles = User.find_by(email: current_user.email).roles
		end
		roles.each do |r|
			sub = r.portals.collect{|p| p.name}.uniq
			@portals << {url: "/portal/#{r.role.downcase}", name: "#{r.role} Portal", sub: sub}
		end
	end

	def school_code
		code = request.host.sub(/.myschooldismissal.com.*/, '').downcase
		return 'www' if code == 'www'
		s = School.find_by(code: code)
		unless s.nil? or s.users.include? current_user
			sign_out and return
		end
	  redirect_to 'https://www.myschooldismissal.com' and return if s.nil?
	  render 'gen' and return unless s.configured
		return code
	end

	private

	  def allow_iframe
			response.headers.except! 'X-Frame-Options'
		end
end
