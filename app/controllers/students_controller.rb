class StudentsController < ApplicationController
  require 'will_paginate/array'
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    redirect_to root_path and return unless current_user.roles.include? Role.find(3)
    unconfirmed = []
    StudentUser.where('NOT confirmed and teach').each do |su|
      student = {}
      student[:id] = su.student.id
      student[:name] = su.student.name
      student[:status] = su.student.status
      student[:teach] = su.user
      unconfirmed << student
    end

    @unconfirmed_teach = unconfirmed

    students = []
    StudentUser.where('confirmed and teach').each do |su|
      student = {}
      student[:id] = su.student.id
      student[:name] = su.student.name
      student[:status] = su.student.status
      student[:teach] = su.user
      students << student
    end
    @students_teach = students

    unconfirmed = []
    StudentUser.where('NOT confirmed and guard').each do |su|
      student = {}
      student[:id] = su.student.id
      student[:name] = su.student.name
      student[:status] = su.student.status
      student[:guard] = su.user
      unconfirmed << student
    end

    @unconfirmed = unconfirmed

		students = []
    StudentUser.where('confirmed and guard').each do |su|
      student = {}
      student[:id] = su.student.id
      student[:name] = su.student.name
      student[:status] = su.student.status
      student[:guard] = su.user
      students << student
    end
    @students = students

		@all_students = Student.all.map{|s| [s.name, s.id]}
		@all_users = User.all.map{|u| [u.name, u.id]}

		respond_to do |format|
			format.html { render 'index' }
			format.json { render json: {allowed: students, unconfirmed: unconfirmed} }
		end
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

	def bind
		su = StudentUser.new(student_id: params[:student_id], user_id: params[:user_id], guard: true)
		redirect_to students_path and return if su.save!
		render plain: 'ERROR: unknown error occurred' and return
	end

  def confirm
    user = User.find_by(id: params[:uid])
    render plain: 'ERROR: User not found' and return if user.nil?
    if params[:role] == 'guard'
      students = user.student_users.select{|s| s.student_id == params[:sid].to_i and s.guard == true}
    elsif params[:role] == 'teach'
      students = user.student_users.select{|s| s.student_id == params[:sid].to_i and s.teach == true}
    else
      render plain: "ERROR: unknown role #{params[:role]}" and return
    end
    render plain: 'ERROR: Student not found' and return if students.nil? or students.empty?
    render plain: 'ERROR: database corrupted' and return if students.count > 1
    student = students.first
    render plain: "ERROR: unknown error occurred with #{students}" and return if student.nil?
    render plain: 'OK' and return if student.update(confirmed: true)
    render plain: 'ERROR: unknown error occurred' and return
  end

	def unbind
    if params[:role] == 'guard'
			su = StudentUser.where(user_id: params[:uid], student_id: params[:sid], guard: true)
    elsif params[:role] == 'teach'
			su = StudentUser.where(user_id: params[:uid], student_id: params[:sid], teach: true)
    else
      render plain: "ERROR: unknown role #{params[:role]}" and return
    end
    render plain: "ERROR: User (#{params[:uid]}) or student (#{params[:sid]})not found (in that capacity - #{params[:role]})" and return if su.nil?
    render plain: 'OK' and return if su.first.delete
    render plain: 'ERROR: unknown error occurred' and return
		render json: {unbind: params[:uid], from: params[:sid]} and return
	end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:name, :status)
    end
end
