class AdminController < ApplicationController
  def index
    redirect_to root_path and return unless User.find_by(email: current_user.email).roles.collect{|r| r.role}.include?('Faculty')
    redirect_to students_path
		@users = School.find_by(code: school_code).users.select{|u| u.email != current_user.email}
		@students = []
  end

  def users
    redirect_to root_path and return unless User.find_by(email: current_user.email).roles.collect{|r| r.role}.include?('Faculty')
  	@users = User.where('id != ?', current_user.id).order(name: :asc)
  end

	def change_role
    redirect_to root_path and return unless User.find_by(email: current_user.email).roles.collect{|r| r.role}.include?('Faculty')
		user = User.find_by(id: params[:uid])
		render plain: "ERROR user not found" and return if user.nil?
		role = Role.find_by(id: params[:rid])
		role = Role.find_by(role: params[:rid]) if role.nil?
		render plain: "ERROR, role not found" and return if role.nil?

		if params[:add] == 'true'
			render plain: "ERROR: Can't add admin to yourself." and return if user.id == current_user.id and role.id == 3
			user.roles <<  role
			render plain: "Added role #{role.role} to user #{user.name}."
		else
			render plain: "ERROR: Can't remove admin from yourself." and return if user.id == current_user.id and role.id == 3
			user.roles.delete(role)
			render plain: "Removed role #{role.role} from user #{user.name}."
		end
	end

	def user_delete
    redirect_to root_path and return unless User.find_by(email: current_user.email).roles.collect{|r| r.role}.include?('Faculty')
		render html: "<script>alert('You cannot delete yourself'); javascript.history(-1);</script>".html_safe and return if current_user.id == params[:uid]
		u = User.find_by(id: params[:uid])
		render plain: 'User NOT FOUND' and return if u.nil?
		u.teaching_students.delete_all
		u.guarding_students.delete_all
		u.vehicles.delete_all
		u.roles.delete_all
		u.delete
		render html: "<script>alert('User deleted'); ;</script>".html_safe
	end
end
