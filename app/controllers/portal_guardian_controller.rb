class PortalGuardianController < ApplicationController
  def index
    redirect_to portal_guardian_students_path
  end

  def students
    @students = User.find_by(email: current_user.email).confirmed_students.sort_by{|s| s.name}
    @unconfirmed = User.find_by(email: current_user.email).unconfirmed_students
    respond_to do |format|
      format.html { render 'students' }
      format.json {
        allowed = @students.collect{|s|
					status = s.status
					history = History.where('created_at >= ?', Time.zone.now.beginning_of_day).select{|hs| hs.student_id == s.id}.last
					unless history.nil?
						status = history.status
					end
					{id: s.id, name: s.name, status: status}
				}
        default = User.find_by(email: current_user.email).default_guarding_students.collect{|d| d.id}
				unconfirmed = @unconfirmed.collect{|s|
					{id: s.id, name: s.name}
				}
        render json: {allowed: allowed, unconfirmed: unconfirmed, default: default }
      }
    end
  end

  def students_new
		@student = Student.new
	end

  def students_create
		s = Student.find_or_create_by(name: params[:student][:name])
		current_user.unconfirmed_students << s
		respond_to do |format|
			format.html { redirect_to portal_guardian_students_path }
			format.json { render json: {status: 'OK'} }
		end
	end

  def students_update
		s = Student.find_by(id: params[:id])
		render plain: "ERROR: student #{params[:id]} not found" and return if s.nil?
		render plain: 'ERROR: permission denied, student has already been confirmed' and return if User.find_by(email: current_user.email).confirmed_students.include? s
		if s.update(name: params[:student][:name])
			respond_to do |format|
				format.html { redirect_to portal_guardian_students_path }
				format.json { render json: {status: 'OK'} }
			end
		else
			render plain: 'error' #TODO: make this human readable
		end
	end

  def vehicles_new
		@vehicle = Vehicle.new
	end

  def vehicles_create
		v = Vehicle.find_by(plate: params[:vehicle][:plate])
		unless v.nil?
			current_user.vehicles << v
			render plain: 'That vehicle already exists and has been added to your vehicles' and return
		end
		v = Vehicle.new(plate: params[:vehicle][:plate], description: params[:vehicle][:description])
		if v.save! and 
			current_user.vehicles << v
			respond_to do |format|
				format.html { redirect_to portal_guardian_vehicles_path }
				format.json { render plain: 'Vehicle created' and return }
			end
		else
			render plain: 'error' #TODO: make this human readable
		end
	end

  def vehicles_update
		v = Vehicle.find_by(id: params[:id])
		render plain: "ERROR: vehicle #{params[:id]} not found" and return if v.nil?
		if v.update(plate: params[:vehicle][:plate], description: params[:vehicle][:description])
			respond_to do |format|
				format.html { redirect_to portal_guardian_vehicles_path }
				format.json { 
					vehicles = User.find_by(email: current_user.email).vehicles.collect{|v| {id: v.id, plate: v.plate, description: v.description}}
					default_vehicle = VehicleUser.find_by(user_id: current_user.id, default: true)
					default_vehicle = default_vehicle.vehicle_id unless default_vehicle.nil?
					render json: { known: vehicles, default: default_vehicle } and return
				}
			end
		end
		render plain: 'error' and return #TODO: make this human readable
	end

  def vehicles_delete
		v = Vehicle.find_by(id: params[:id])
		render plain: "ERROR: vehicle #{params[:id]} not found" and return if v.nil?
		if v.users.delete(User.find_by(email: current_user.email))
			v.delete if v.users.count == 0
			respond_to do |format|
				format.html { redirect_to portal_guardian_vehicles_path }
				format.json { 
					vehicles = User.find_by(email: current_user.email).vehicles.collect{|v| {id: v.id, plate: v.plate, description: v.description}}
					default_vehicle = VehicleUser.find_by(user_id: current_user.id, default: true)
					default_vehicle = default_vehicle.vehicle_id unless default_vehicle.nil?
					render json: { known: vehicles, default: default_vehicle } and return
				}
			end
		end
		render plain: 'error' and return #TODO: make this human readable
	end

  def vehicles
    @vehicles = User.find_by(email: current_user.email).vehicles
    respond_to do |format|
      format.html { render 'vehicles' }
      format.json {
        vehicles = @vehicles.collect{|v| {id: v.id, plate: v.plate, description: v.description}}
        default_vehicle = VehicleUser.find_by(user_id: current_user.id, default: true)
        default_vehicle = default_vehicle.vehicle_id unless default_vehicle.nil?
        render json: {known: vehicles, default: default_vehicle}
      }
    end
  end
end
