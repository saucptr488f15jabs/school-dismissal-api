class HomeController < ApplicationController
	skip_before_filter :authenticate_user!
  def index
		if current_user.nil?
			render 'public'
		else
# 			p current_user.methods
# 			redirect_to link_school_path and return if current_user.schools.nil?
			redirect_to portal_teacher_status_path and return if is_local?

			roles = User.find_by(email: current_user.email).roles.collect{|r| r.role}
			redirect_to "/portal/#{roles.first.downcase}" and return if roles.count == 1

			render 'private'
		end
  end

  def client_logo
		render json: {img: nil} and return unless File.exists? 'public/client_logo.jpg'
  	img = Base64.encode64(open("public/client_logo.jpg") {|io| io.read})
  	img = "data:image/jpg;base64,#{img}"
  	render json: {"img": img} and return
  end
end
