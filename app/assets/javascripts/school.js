function genCode(name) {
	code = name.replace(/\W*(\w)\w*/g, '$1').toUpperCase()
	document.getElementById('school_code').value = code;
	document.getElementById('url').innerHTML = '<a href="http://'+code.toLowerCase()+'.myschooldismissal.com">'+code.toLowerCase()+'.myschooldismissal.com</a>';
	$('#code_explain').show();
	testCode(code);
}

function testCode(code) {
	$.ajax({
		url: '/school/check_code/' + code,
		type: 'GET',
		success: function(data) {
			document.getElementById('school_code').value = data;
		}
	});
}
