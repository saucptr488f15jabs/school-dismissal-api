function start_status_view() {
    var do_update = false;
    $.getJSON('/pick_up/teacher', function(data) {
        $.each(data, function(key,val) {
            var absent = (val.status == 'absent') ? ' checked=checked' : '';
            var td_str = '<td><input type="checkbox" name="absent" id="absent'+val.id+'" onchange="javascript:update_attendance('+val.id+');"></td>';
            td_str += '<td>' + val.name + '</td>';
            td_str += '<td>' + val.status + '</td>';
            td_str += '<td>' + val.guardian + '</td>';
            td_str += '<td>' + val.location + '</td>';
            td_str += '<td><a href="javascript:update_status('+val.id+');">Dismissed</a></td>';
            if($('#status_view_'+key).length == 0) {
                $('#status_view tr:last').after('<tr id="status_view_'+key+'">' + td_str + '</tr>');
                do_update = true;
            } else {
                $('#status_view_'+key).html(td_str);
            }
            if(val.status == "Absent") {
                document.getElementById("absent" + val.id).checked=true;
            }
        });
        if(do_update) {update_attendance();}
    });
    setTimeout('start_status_view()', 3000);
}

function update_attendance(id) {
    dataj = {status: "Absent"};
    datas = JSON.stringify(dataj);
    if(id != undefined) {
        console.log("Update status for " + id + " to " + dataj);
        $.ajax({
            url: '/portal/teacher/update/'+id,
            type: 'PUT',
            contentType: 'application/json',
            accept: 'application/json',
            data: datas,
            sync: false
        }).success(function(data) {
            console.log(data);
        });
    }
}

function update_status(id) {
    dataj = {status: "En Route"};
    datas = JSON.stringify(dataj);
    console.log("Update status for " + id + " to " + dataj);
    $.ajax({
        url: '/portal/teacher/update/'+id,
        type: 'PUT',
        contentType: 'application/json',
        accept: 'application/json',
        data: datas,
        sync: false
    }).success(function(data) {
        console.log('OK');
    });
}
function resetAll() {
    $.ajax({
        url: '/portal/teacher/reset',
        type: 'PUT',
        contentType: 'application/json',
        accept: 'application/json',
        sync: false
    }).success(function(data) {
        console.log(data);
    });
}