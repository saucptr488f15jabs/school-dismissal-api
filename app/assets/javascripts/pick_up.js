$(document).ready(function() {
    $('#pick_up_sub').click(function (event) {
        var students = $('input[name=pick_up]:checked').map(function() {
            return this.value;
        }).get();
        var vehicle = $('input[name=vehicle]:checked').val();
        var location = $('input[name=location]:checked').val();
        dataj = { students: students, vehicle: vehicle, location: location };
        datas = JSON.stringify(dataj);
        console.log(datas);
        $.ajax({
            url: '/pick_up',
            type: 'POST',
            contentType: 'application/json',
            accept: 'application/json',
            data: datas,
            sync: false
        }).success(function(data) {
            window.location.reload(true);
        });
        event.preventDefault();
    });
});
