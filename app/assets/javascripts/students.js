function confirm(student, parent, role) {
	$.ajax({
		url: '/portal/faculty/students/confirm/',
		type: 'PUT',
		data: "sid=" + student + "&uid=" + parent + "&role=" + role,
		success: function(data) {
			window.location.reload();
		}
	});
}
function unbind(student, parent, role) {
	$.ajax({
		url: '/portal/faculty/students/unbind/',
		type: 'PUT',
		data: "sid=" + student + "&uid=" + parent + "&role=" + role,
		success: function(data) {
			window.location.reload();
		}
	});
}
