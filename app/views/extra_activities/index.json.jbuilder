json.array!(@extra_activities) do |extra_activity|
  json.extract! extra_activity, :id, :ActName
  json.url extra_activity_url(extra_activity, format: :json)
end
