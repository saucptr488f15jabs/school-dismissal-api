json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :plate, :description
  json.url vehicle_url(vehicle, format: :json)
end
