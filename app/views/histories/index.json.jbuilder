json.array!(@histories) do |history|
  json.extract! history, :id, :status, :location
  json.url history_url(history, format: :json)
end
