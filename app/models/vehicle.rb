class Vehicle < ActiveRecord::Base
  validates :plate, uniqueness: true
  
  has_many :vehicle_users
  has_many :users, through: :vehicle_users
end
