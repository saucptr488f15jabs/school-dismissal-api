class VehicleUser < ActiveRecord::Base
  validates :user_id, uniqueness: {scope: :vehicle_id}

  belongs_to :vehicle
  belongs_to :user
end
