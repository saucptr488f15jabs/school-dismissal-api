class StudentUser < ActiveRecord::Base
  validates :user_id, uniqueness: {scope: [:student_id, :guard]}
  validates :user_id, uniqueness: {scope: [:student_id, :teach]}

  belongs_to :student
  belongs_to :user
end
