class History < ActiveRecord::Base
  belongs_to :student
  belongs_to :user
  belongs_to :location
  belongs_to :vehicle
end
