class Student < ActiveRecord::Base
  validates :name, uniqueness: true

  has_many :student_users
  has_many :users, through: :student_users

  has_many :student_activities
  has_many :activities, through: :student_activities
  
  has_many :histories

  has_many :teaching_users, -> { where(student_users: {teach: true}) }, through: :student_users, source: :user
  has_many :guarding_users, -> { where(student_users: {guard: true}) }, through: :student_users, source: :user
end
