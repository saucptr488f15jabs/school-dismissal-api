class Activity < ActiveRecord::Base
  validates :name, uniqueness: true

  has_many :student_activities
  has_many :students, through: :student_activities
end