class SchoolUser < ActiveRecord::Base
  validates :user_id, uniqueness: {scope: :school_id}

  belongs_to :school
  belongs_to :user
end
