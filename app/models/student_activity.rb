class StudentActivity < ActiveRecord::Base
  validates :student_id, uniqueness: {scope: :activity_id}

  belongs_to :student
  belongs_to :activity
end
