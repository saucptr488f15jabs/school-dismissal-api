class School < ActiveRecord::Base
  validates :name, uniqueness: true
  validates :email, uniqueness: true
  validates :code, uniqueness: true
  
  has_many :school_users
  has_many :users, through: :school_users
end
