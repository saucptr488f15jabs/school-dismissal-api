class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
				 :token_authenticatable
  has_many :authentication_tokens

  validates :email, uniqueness: true
  # validates :reset_password_token, uniqueness: true

  has_many :user_roles
  has_many :roles, through: :user_roles

  has_many :vehicle_users
  has_many :vehicles, through: :vehicle_users

	has_many :school_users
	has_many :schools, through: :school_users

  has_many :student_users
  has_many :students, through: :student_users
  
  has_many :histories

  has_many :teaching_students, -> { where(student_users: {teach: true}) }, through: :student_users, source: :student

  has_many :unconfirmed_students, -> { where(student_users: {guard: true, confirmed: false}) }, through: :student_users, source: :student
  has_many :confirmed_students, -> { where(student_users: {guard: true, confirmed: true}) }, through: :student_users, source: :student

  has_many :default_teaching_students, -> { where(student_users: {default_teach: true}) }, through: :student_users, source: :student
  has_many :default_guarding_students, -> { where(student_users: {default_guard: true, confirmed: true}) }, through: :student_users, source: :student
end
