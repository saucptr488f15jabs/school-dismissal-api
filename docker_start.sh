#!/bin/bash
rm -v tmp/pids/server.pid 2>/dev/null
echo "============== Running bundler =============="
bundle
echo "============== Starting Server =============="
rails s -b 0.0.0.0
echo "============== Server Stopped  =============="
sleep 999
