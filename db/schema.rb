# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160905173526) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "start_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "activities", ["name"], name: "index_activities_on_name", unique: true, using: :btree

  create_table "authentication_tokens", force: :cascade do |t|
    t.string   "body"
    t.integer  "user_id"
    t.datetime "last_used_at"
    t.string   "ip_address"
    t.string   "user_agent"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "authentication_tokens", ["user_id"], name: "index_authentication_tokens_on_user_id", using: :btree

  create_table "histories", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "user_id"
    t.integer  "vehicle_id"
    t.integer  "location_id"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "histories", ["location_id"], name: "index_histories_on_location_id", using: :btree
  add_index "histories", ["student_id"], name: "index_histories_on_student_id", using: :btree
  add_index "histories", ["user_id"], name: "index_histories_on_user_id", using: :btree
  add_index "histories", ["vehicle_id"], name: "index_histories_on_vehicle_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "locations", ["name"], name: "index_locations_on_name", unique: true, using: :btree

  create_table "portals", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "portals", ["role_id"], name: "index_portals_on_role_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "school_users", force: :cascade do |t|
    t.boolean "default_school", default: false, null: false
    t.integer "user_id"
    t.integer "school_id"
  end

  add_index "school_users", ["school_id"], name: "index_school_users_on_school_id", using: :btree
  add_index "school_users", ["user_id", "school_id"], name: "index_school_users_on_user_id_and_school_id", unique: true, using: :btree
  add_index "school_users", ["user_id"], name: "index_school_users_on_user_id", using: :btree

  create_table "schools", force: :cascade do |t|
    t.string   "name",                       null: false
    t.string   "owner",                      null: false
    t.string   "email"
    t.string   "address",                    null: false
    t.string   "city",                       null: false
    t.string   "state",                      null: false
    t.integer  "zip"
    t.string   "phone"
    t.string   "code",                       null: false
    t.boolean  "configured", default: false, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "schools", ["code"], name: "index_schools_on_code", unique: true, using: :btree
  add_index "schools", ["email"], name: "index_schools_on_email", unique: true, using: :btree
  add_index "schools", ["name"], name: "index_schools_on_name", unique: true, using: :btree

  create_table "student_activities", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "activity_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "student_activities", ["activity_id"], name: "index_student_activities_on_activity_id", using: :btree
  add_index "student_activities", ["student_id", "activity_id"], name: "index_student_activities_on_student_id_and_activity_id", unique: true, using: :btree
  add_index "student_activities", ["student_id"], name: "index_student_activities_on_student_id", using: :btree

  create_table "student_users", force: :cascade do |t|
    t.boolean  "guard",         default: false, null: false
    t.boolean  "teach",         default: false, null: false
    t.boolean  "default_guard", default: false, null: false
    t.boolean  "default_teach", default: false, null: false
    t.boolean  "confirmed",     default: false, null: false
    t.integer  "user_id"
    t.integer  "student_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "student_users", ["student_id"], name: "index_student_users_on_student_id", using: :btree
  add_index "student_users", ["user_id", "student_id", "guard"], name: "index_student_users_on_user_id_and_student_id_and_guard", unique: true, using: :btree
  add_index "student_users", ["user_id", "student_id", "teach"], name: "index_student_users_on_user_id_and_student_id_and_teach", unique: true, using: :btree
  add_index "student_users", ["user_id"], name: "index_student_users_on_user_id", using: :btree

  create_table "students", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "school"
  end

  add_index "students", ["name"], name: "index_students_on_name", unique: true, using: :btree

  create_table "user_roles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id", using: :btree
  add_index "user_roles", ["user_id", "role_id"], name: "index_user_roles_on_user_id_and_role_id", unique: true, using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                   default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  create_table "vehicle_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "vehicle_id"
    t.boolean "default",    default: false, null: false
  end

  add_index "vehicle_users", ["user_id", "vehicle_id"], name: "index_vehicle_users_on_user_id_and_vehicle_id", unique: true, using: :btree
  add_index "vehicle_users", ["user_id"], name: "index_vehicle_users_on_user_id", using: :btree
  add_index "vehicle_users", ["vehicle_id"], name: "index_vehicle_users_on_vehicle_id", using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.string   "plate",       null: false
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "vehicles", ["plate"], name: "index_vehicles_on_plate", unique: true, using: :btree

  add_foreign_key "authentication_tokens", "users"
  add_foreign_key "histories", "locations"
  add_foreign_key "histories", "students"
  add_foreign_key "histories", "users"
  add_foreign_key "histories", "vehicles"
  add_foreign_key "portals", "roles"
  add_foreign_key "school_users", "schools"
  add_foreign_key "school_users", "users"
  add_foreign_key "student_activities", "activities"
  add_foreign_key "student_activities", "students"
  add_foreign_key "student_users", "students"
  add_foreign_key "student_users", "users"
  add_foreign_key "user_roles", "roles"
  add_foreign_key "user_roles", "users"
  add_foreign_key "vehicle_users", "users"
  add_foreign_key "vehicle_users", "vehicles"
end
