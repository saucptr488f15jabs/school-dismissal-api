class CreateVehicleUsers < ActiveRecord::Migration
  def change
    create_table :vehicle_users do |t|
      t.references :user, index:true, foreign_key: true
      t.references :vehicle, index:true, foreign_key: true
			t.boolean :default, null: false, default: false
    end
    add_index :vehicle_users, [:user_id, :vehicle_id], unique: true
  end
end
