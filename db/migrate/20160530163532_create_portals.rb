class CreatePortals < ActiveRecord::Migration
  def change
    create_table :portals do |t|
      t.string :name
      t.string :url
	  t.references :role, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
