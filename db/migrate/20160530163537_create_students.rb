class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name, null: false
      t.string :status

      t.timestamps null: false
    end
	add_index :students, :name, unique: true
  end
end
