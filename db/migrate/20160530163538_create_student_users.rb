class CreateStudentUsers < ActiveRecord::Migration
  def change
    create_table :student_users do |t|
      t.boolean :guard, null: false, default: false
      t.boolean :teach, null: false, default: false
      t.boolean :default_guard, null: false, default: false
      t.boolean :default_teach, null: false, default: false
      t.boolean :confirmed, null: false, default: false
      t.references :user, index:true, foreign_key: true
      t.references :student, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :student_users, [:user_id, :student_id, :guard], unique: true
    add_index :student_users, [:user_id, :student_id, :teach], unique: true
  end
end
