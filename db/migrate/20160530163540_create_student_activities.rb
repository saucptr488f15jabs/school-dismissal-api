class CreateStudentActivities < ActiveRecord::Migration
  def change
    create_table :student_activities do |t|
      t.references :student, index: true, foreign_key: true
      t.references :activity, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :student_activities, [:student_id, :activity_id], unique: true
  end
end
