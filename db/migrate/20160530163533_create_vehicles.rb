class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :plate, null: false
      t.string :description

      t.timestamps null: false
    end
    add_index :vehicles, :plate, unique: true
  end
end
