class CreateSchoolUsers < ActiveRecord::Migration
  def change
    create_table :school_users do |t|
      t.boolean :default_school, null: false, default: false
      t.references :user, index:true, foreign_key: true
      t.references :school, index:true, foreign_key: true
    end
    add_index :school_users, [:user_id, :school_id], unique: true
  end
end
