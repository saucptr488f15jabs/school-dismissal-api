class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name, null: false
      t.string :owner, null: false
      t.string :email
      t.string :address, null: false
      t.string :city, null: false
      t.string :state, null: false
      t.integer :zip
      t.string :phone
      t.string :code, null: false
      t.boolean :configured, null: false, default: false

      t.timestamps null: false
    end
    add_index :schools, :name, unique: true
    add_index :schools, :email, unique: true
    add_index :schools, :code, unique: true
  end
end
