# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

print "Roles: "
guard = Role.create(role: 'Parent')
teacher = Role.create(role: 'Teacher')
admin = Role.create(role: 'Faculty')
puts "done"

print "Location: "
Location.create(name: 'Front')
Location.create(name: 'Gym')
Location.create(name: 'Church')
puts "done"

print "Portals: "
# pstudent = Portal.create(name: 'students')
# pvehicle = Portal.create(name: 'vehicles')
# pstatus = Portal.create(name: 'status')
# pteacher = Portal.create(name: 'teachers')
# pactivity = Portal.create(name: 'extra_activities')
# pguardian = Portal.create(name: 'guardians')
guard.portals << Portal.create(name: 'students')
guard.portals << Portal.create(name: 'vehicles')
teacher.portals << Portal.create(name: 'status')
# teacher.portals << Portal.create(name: 'students')
admin.portals << Portal.create(name: 'students')
admin.portals << Portal.create(name: 'vehicles')
admin.portals << Portal.create(name: 'teachers')
# admin.portals << Portal.create(name: 'extra_activities')
admin.portals << Portal.create(name: 'teachers')
puts "done"

if Rails.env == "development"
	print "Students: "
	s1 = Student.create(name: "kid1")
	s2 = Student.create(name: "kid2")
	puts "done"

	print "Vehicles: "
	v1 = Vehicle.create(plate: "abc123", description: "old car")
	v2 = Vehicle.create(plate: "qwer456", description: "new car")
	puts "done"

	print "Schools: "
	beta = School.create(name: "Beta", owner: "Austin Covrig", email: "myschooldismissal@gmail.com", address: "1234 booring road", city: "Nowhere", state: "AB", code: "beta", configured: true)
	aws = School.create(name: 'aws', owner: 'Austin Test', address: 'asdf', city: 'asdf', state: 'TN', code: 'aws', configured: true)
	puts "done"

	print "Users: "
	u = User.create(name: "Austin Covrig", email: "accovrig@gmail.com", password: "asdf1234", password_confirmation: "asdf1234", reset_password_token: Devise.friendly_token)
	u.roles << guard
	u.roles << teacher
	u.roles << admin
	u.teaching_students << s1
	u.teaching_students << s2
	u.confirmed_students << s1
	u.confirmed_students << s2
	u.vehicles << v1
	u.vehicles << v2
	beta.users << u
	aws.users << u
	u = User.create(name: "Josh Wilkins", email: "joshw@iiw.org", password: "asdf1234", password_confirmation: "asdf1234", reset_password_token: Devise.friendly_token)
	u.roles << guard
	u.roles << teacher
	u.roles << admin
	u.teaching_students << s1
	u.teaching_students << s2
	u.confirmed_students << s1
	u.confirmed_students << s2
	u.vehicles << v1
	u.vehicles << v2
	beta.users << u
	aws.users << u
	puts "done"
end
