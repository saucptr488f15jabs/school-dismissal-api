require 'selenium-webdriver'
require 'ripl'

print 'Launching driver: '
driver = Selenium::WebDriver.for :firefox
puts 'OK'
print 'Loading main page: '
driver.navigate.to "https://myschooldismissal.com"
puts 'OK'

print 'Logging in: '
element = driver.find_element(:id, 'user_email')
element.send_keys "accovrig@gmail.com"
element = driver.find_element(:id, 'user_password')
element.send_keys "1234"
element.submit
puts 'OK'

puts '========= Testing starts here =========='
print 'Testing first child: '
driver.navigate.to "https://myschooldismissal.com/portal/guardian/students"
child = driver.find_element(:xpath, '/html/body/table/tbody/tr[2]/td[1]').text
if child != "Amethyst Pope"
	return "ERROR, the first child (#{child}) is not \"Amethyst Pope\"! Something must be wrong"
	Ripl.start binding: binding
	exit
else
	puts 'OK'
end



puts '========= Testing ends here =========='

puts ''
puts 'Ready to create new tests: '
Ripl.start binding: binding
driver.quit
exit