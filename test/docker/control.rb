require 'selenium-webdriver'
require 'ripl'
require 'headless'
require 'rspec'

puts "ERROR: Missing argument: browser <firefox|chrome>, defaulting to Chrome" if ARGV.empty?
def setup
	@wait = Selenium::WebDriver::Wait.new(:timeout => 10)

	print "Starting #{@name}: "
	if not @both
		print "headless "
		@headless = Headless.new(dimensions: "1680x1050x24", video: { :provider => :ffmpeg, :codec => 'libx264' })
		print "start "
		@headless.start
		Process.fork do
			`x11vnc >/dev/null 2>/dev/null`
		end
	end

	@driver = Selenium::WebDriver.for :chrome if @name == 'Chrome'
	@driver = Selenium::WebDriver.for :firefox if @name == 'Firefox'
	@headless.video.start_capture if not @both

	puts 'OK'
end

def teardown
	@headless.video.stop_and_save('/test/tests.mp4')
	@driver.quit
	@headless.destroy
	puts 'All tests completed.'
end

def run
	@name='Unknown'
	if ARGV[0] == "chrome"
		@name='Chrome'
	elsif ARGV[0] == "firefox"
		@name='Firefox'
	else
		@name = 'Chrome'
	end

	if ARGV[0].nil?
		@name='Chrome'
		setup
		yield
		@driver.quit

#		@both=true
#		@name='Firefox'
#		setup
#		yield
	else
		@both=false
		setup
		yield
	end
	teardown
end

run do
	print 'Loading main page: '
	@driver.navigate.to "http://www.myschooldismissal.com/"
	puts 'OK'

# gets

	print 'Logging in: '
	element = @driver.find_element(:id, 'user_email')
	element.send_keys "accovrig@gmail.com"
	element = @driver.find_element(:id, 'user_password')
	element.send_keys "1234"
	element.submit
	puts 'OK'

	puts '========= Testing starts here =========='
	print 'Testing first child: '
	@wait.until { @driver.find_elements(:xpath, "//a[@href='/portal/guardian']").size() > 0 }
	@driver.find_element(:xpath, "//a[@href='/portal/guardian']").click
	@wait.until { @driver.find_elements(:xpath, "//a[@href='/portal/guardian/students']").size() > 0 }
	@driver.find_element(:xpath, "//a[@href='/portal/guardian/students']").click

	@wait.until { @driver.find_elements(:xpath, '/html/body/table/tbody/tr[2]/td[1]').size() > 0 }
	child = @driver.find_element(:xpath, '/html/body/table/tbody/tr[2]/td[1]').text
	if child != "Amethyst Pope"
		puts "ERROR, the first child (#{child}) is not \"Amethyst Pope\"! Something must be wrong"
# 		Ripl.start binding: binding
# 		exit
	else
		puts 'OK'
	end

	print 'Testing edit button: '
	@wait.until { @driver.find_elements(:xpath, "/html/body/table/tbody/tr[2]/td[4]/a[1]").size() > 0 }
	@driver.find_element(:xpath, "/html/body/table/tbody/tr[2]/td[4]/a[1]").click

	@wait.until { @driver.find_elements(:id, 'student_name').size() > 0 }
	child = @driver.find_element(:id, 'student_name').attribute('value')
	if child != "Amethyst Pope"
		puts "ERROR, the first child (#{child}) is not \"Amethyst Pope\"! Something must be wrong"
# 		Ripl.start binding: binding
# 		exit
	else
		puts 'OK'
	end

	@driver.save_screenshot 'headless.png'

	puts '========= Testing ends here =========='

	puts ''
	puts 'Ready to create new tests: '
	Ripl.start binding: binding
	puts 'done'
end
