Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  root 'home#index'
  get 'home/index', to: 'home#index'
  get 'client_logo', to: 'home#client_logo'

  get 'error/js'

  get 'pick_up', to: 'pick_up#index'
  put 'pick_up', to: 'pick_up#update'
  post 'pick_up', to: 'pick_up#update'
  get 'pick_up/teacher', to: 'pick_up#teacher', as: 'pick_up_teacher'
  post 'pick_up/attendance', to: 'pick_up#attendance'

  scope '/school' do
    get 'register', to: 'school#register', as: 'new_school'
    post 'register', to: 'school#save'
    get 'check_code/:code', to: 'school#check_code'
    get 'link', to: 'school#link', as: 'link_school'
  end

  get '/locations', to: 'locations#index'

  scope '/portal' do
    get 'parent', to: 'portal_guardian#index', as: 'portal_guardian_index'
    scope 'parent' do
      get 'students', to: 'portal_guardian#students', as: 'portal_guardian_students'
      get 'students/new', to: 'portal_guardian#students_new', as: 'portal_guardian_students_new'
      post 'students/new', to: 'portal_guardian#students_create'
      put 'students/:id', to: 'portal_guardian#students_update'
      get 'vehicles', to: 'portal_guardian#vehicles', as: 'portal_guardian_vehicles'
      get 'vehicles/new', to: 'portal_guardian#vehicles_new', as: 'portal_guardian_vehicles_new'
      post 'vehicles/new', to: 'portal_guardian#vehicles_create'
      put 'vehicles/:id', to: 'portal_guardian#vehicles_update'
      delete 'vehicles/:id', to: 'portal_guardian#vehicles_delete'
    end

    get 'teacher', to: 'portal_teacher#index', as: 'portal_teacher_index'
    scope 'teacher' do
      get 'status', to: 'portal_teacher#status', as: 'portal_teacher_status'
      get 'students', to: 'portal_teacher#students', as: 'portal_teacher_students'
      get 'students/new', to: 'portal_teacher#students_new', as: 'portal_teacher_students_new'
      post 'students/new', to: 'portal_teacher#students_create'

      put 'reset', to: 'portal_teacher#reset'
      put 'update/:student_id', to: 'portal_teacher#update' # This for legacy version of API
      put 'students/:student_id', to: 'portal_teacher#update', as: 'portal_teacher_update'
    end

    get 'faculty', to: 'admin#index', as: 'admin'
    scope 'faculty' do
      # resources :admins
      # resources :extra_activities
      resources :vehicles
      # resources :users
      resources :guardians
      get 'users', to: 'admin#users', as: 'admin_users'
      resources :students do
      	collection do
					post :bind, as: 'bind'
      		put :confirm, as: 'confirm'
					put :unbind, as: 'unbind'
      	end
      end
      resources :locations
      # resources :histories
      put '/change_role/:add', to: 'admin#change_role'
      delete 'teacher/:id', to: 'teachers#destroy', as: 'remove_teacher'
    end
  end

  delete '/admin/user/:uid', to: 'admin#user_delete', as: 'user_delete'
end
