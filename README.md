# About
This is a project specifically for Senior Project by Austin Covrig, Shaina Guiñes, Brandon Underwood, and Josh Wilkins. This project is intended to allow parents to notify teachers when they arrive to pick up their children.

# How it works
In our design, there is are 4 components:
- Server
- Parent Interface
- Teacher Portal
- Faculty Portal
We intend to offer the components at around $10/mo. This will allow hosting, support, and maintenance. We also intend to offer the ability for schools to roll their own instance of the server if they want full control or not to pay the monthly fee to maintain the server.

# Requirements
- [Docker](https://www.docker.com/) - 
You can install this with apt-get/yum or install the [Docker Toolbox](https://www.docker.com/docker-toolbox) for mac or windows
- [Docker Compose](https://docs.docker.com/compose/install/) - 
This is used to orchestrate the 3 continers that will be needed.
- [Letsencrypt](https://certbot.eff.org/#ubuntuxenial-other) - 
This is used to generate HTTPS certificates.

# How to run
```bash
git clone https://acovrig@bitbucket.org/saucptr488f15jabs/school-dismissal-api.git msd
cd msd
letsencrypt certonly --standalone -d <subdomain>.myschooldismissal.com # where subdomain is the school code assigned to you
docker-compose up -d msd
docker exec -it postgres bash -c "psql -U postgres -c \"create user msd with login createdb password '8ErozPw0@Rq8';\""
docker exec -it msd bash -c 'RAILS_ENV=production rake db:create db:migrate db:seed"'
mkdir -p /srv/docker/nginx/sites-enabled
cp nginx.conf /srv/docker/nginx
cp myschooldismissal /srv/docker/nginx/sites-enabled/myschooldismissal
# Edit /srv/docker/nginx/sites-enabled/myschooldismissal and set your subdomain (line 12)
docker-compose up -d nginx
```

# Building from source
Not sure why you would do this, but it's possible, so why not...
```bash
git clone https://acovrig@bitbucket.org/saucptr488f15jabs/school-dismissal-api.git school-dismissal-api
cd school-dismissal-api
docker build -t msd .
docker run -itd --name postgres postgres
docker run -itd --name msd --link postgres:postgres msd
docker run -itd -p 80:80/tcp -p 443:443/tcp --link msd:msd nginx
```