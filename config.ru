# This file is used by Rack-based servers to start the application.
require 'rack'
require 'rack/cors'

use Rack::Cors do
  allow do
    origins '*.myschooldismissal.com'            # regular expressions can be used here

    resource '/*', :headers => :any, :methods => :any
  end
end

require ::File.expand_path('../config/environment', __FILE__)
run Rails.application
