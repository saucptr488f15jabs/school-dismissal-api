.PHONY: web
web: db
	docker-compose up -d web

db:
	docker-compose up -d db

clean:
	-docker stop myschooldismissalapi_web_1 myschooldismissalapi_db_1 myschooldismissalapi_test_1
	-docker rm -v myschooldismissalapi_web_1 myschooldismissalapi_db_1 myschooldismissalapi_test_1 
	@echo ""
	@echo ""
	@echo "Run \`make reset\` to also remove the images"
	@echo ""
	@echo ""

reset: clean
	docker rmi schooldismissalapi_web schooldismissalapi_test
